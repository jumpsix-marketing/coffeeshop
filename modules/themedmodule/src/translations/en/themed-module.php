<?php
/**
 * themed module for Craft CMS 3.x
 *
 * Module used to return a branded version of the homepage.
 *
 * @link      bigpxl.com
 * @copyright Copyright (c) 2021 Mark
 */

/**
 * themed en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('themed-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Mark
 * @package   ThemedModule
 * @since     1.0.0
 */
return [
    'themed plugin loaded' => 'themed plugin loaded',
];

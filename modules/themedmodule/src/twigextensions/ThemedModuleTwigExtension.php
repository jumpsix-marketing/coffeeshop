<?php
/**
 * themed module for Craft CMS 3.x
 *
 * Module used to return a branded version of the homepage.
 *
 * @link      bigpxl.com
 * @copyright Copyright (c) 2021 Mark
 */

namespace modules\themedmodule\twigextensions;

use modules\themedmodule\ThemedModule;

use Craft;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators,
 * global variables, and functions. You can even extend the parser itself with
 * node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 *
 * @author    Mark
 * @package   ThemedModule
 * @since     1.0.0
 */
class ThemedModuleTwigExtension extends AbstractExtension
{
    // Public Methods
    // =========================================================================

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'ThemedModule';
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
    * @return array
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('themeConnection', [$this, 'themeConnectionFunction']),
        ];
    }

    /**
     * Parse query params and use them to load in alternative branding.
     * 
     * http://coffeeshop.test/?primaryColor=%23042952&secondaryColor=%23ffffff&tertiaryColor=%2350fd9f
     * http://coffeeshop.test/?primaryColor=#042952&secondaryColor=#50fd9f
     * @param null $text
     *
     * @return string
     */
    public function themeConnectionFunction($text = null)
    {
        $request = Craft::$app->getRequest();

        //Define all of the variables which will be replaced
        $variables = array(
            "--primaryBrandingColor" => $request->getQueryParam("primaryColor"),
            "--accentBrandingColor" => $request->getQueryParam("accentColor"),
            "--darkBrandingColor" => $request->getQueryParam("darkColor"),
            "--lightBrandingColor" => $request->getQueryParam("lightColor"),
            "--defaultTextColor" => $request->getQueryParam("defaultTextColor"),
            "--headingFont" => $request->getQueryParam("headingFont"),
            "--bodyFont" => $request->getQueryParam("bodyFont"),
        );

        //Get the logos
        $logo = $request->getQueryParam("primaryLogo");
        $white_logo = $request->getQueryParam("whiteLogo");
        if(!$logo) {
            $logo = $white_logo;
        }
        if(!$white_logo) {
            $white_logo = $logo;
        }

        //Define all of the images that need to be replaced
        $images = json_encode(explode(",", $request->getQueryParam("images")));

        //Get font variables
        $headingFont = $request->getQueryParam("headingFont");
        $headingFontUrl = $request->getQueryParam("headingFontUrl");
        $headingVariant = $request->getQueryParam("headingVariant");

        $bodyFont = $request->getQueryParam("bodyFont");
        $bodyFontUrl = $request->getQueryParam("bodyFontUrl");
        $bodyVariant = $request->getQueryParam("bodyVariant");

        ?>  
            <script>
                window.onload = function() {
                    <?php //Replace the variables ?>
                    <?php foreach($variables as $key => $value): ?>
                        <?php if($value): ?>
                            document.documentElement.style.setProperty('<?php echo $key; ?>', '<?php echo $value; ?>');
                        <?php endif; ?>
                    <?php endforeach; ?>

                    <?php //Logo replacement ?>
                    <?php if($logo): ?>
                        var logo_url = '<?php echo $logo; ?>';
                        var logos = document.querySelectorAll(".logo");
                        for(var i = 0; i < logos.length; i++) {
                            logos[i].src = logo_url;
                        }
                    <?php endif; ?>

                    <?php if($white_logo): ?>
                        var white_logo_url = '<?php echo $white_logo; ?>';
                        var white_logos = document.querySelectorAll(".white-logo");
                        for(var i = 0; i < white_logos.length; i++) {
                            white_logos[i].src = white_logo_url;
                        }
                    <?php endif; ?>

                    <?php //Image replacement ?>
                    <?php if($logo): ?>
                        var images = <?php echo $images ?>;
                        if(images.length > 0) {
                            var elements = document.querySelectorAll(".background-replacement");
                            for(var i = 0; i < elements.length; i++) {
                                if(typeof images[i] === 'undefined') {
                                    var image = images[images.length - 1];
                                }
                                else {
                                    var image = images[i];
                                }
                                elements[i].style.backgroundImage = 'url(' + image + ')';
                            }
                        }
                    <?php endif; ?>

                    <?php //Dynamically load webfonts ?>
                    <?php if($headingFont || $bodyFont): ?>
                        WebFont.load({
                            google: {
                                families: [
                                    '<?php echo $headingFont; ?>:<?php echo $headingVariant; ?>',
                                    '<?php echo $bodyFont; ?>:<?php echo $bodyVariant; ?>',
                                ]
                            }
                        });
                    <?php endif; ?>

                    <?php //Delete marked elements ?>
                    <?php if($logo): ?>
                        var elements = document.querySelectorAll(".delete");
                        for(var i = 0; i < elements.length; i++) {
                            elements[i].remove();
                        }
                    <?php endif; ?>
                }
            </script>
        <?php
    }
}

<?php
/**
 * themed module for Craft CMS 3.x
 *
 * Module used to return a branded version of the homepage.
 *
 * @link      bigpxl.com
 * @copyright Copyright (c) 2021 Mark
 */

namespace modules\themedmodule\assetbundles\themedmodule;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * ThemedModuleAsset AssetBundle
 *
 * AssetBundle represents a collection of asset files, such as CSS, JS, images.
 *
 * Each asset bundle has a unique name that globally identifies it among all asset bundles used in an application.
 * The name is the [fully qualified class name](http://php.net/manual/en/language.namespaces.rules.php)
 * of the class representing it.
 *
 * An asset bundle can depend on other asset bundles. When registering an asset bundle
 * with a view, all its dependent asset bundles will be automatically registered.
 *
 * http://www.yiiframework.com/doc-2.0/guide-structure-assets.html
 *
 * @author    Mark
 * @package   ThemedModule
 * @since     1.0.0
 */
class ThemedModuleAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * Initializes the bundle.
     */
    public function init()
    {
        // define the path that your publishable resources live
        $this->sourcePath = "@modules/themedmodule/assetbundles/themedmodule/dist";

        // define the dependencies
        $this->depends = [
            CpAsset::class,
        ];

        // define the relative path to CSS/JS files that should be registered with the page
        // when this asset bundle is registered
        $this->js = [
            'js/ThemedModule.js',
        ];

        $this->css = [
            'css/ThemedModule.css',
        ];

        parent::init();
    }
}

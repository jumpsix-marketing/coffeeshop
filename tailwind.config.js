module.exports = {
    future: {
      // removeDeprecatedGapUtilities: true,
      // purgeLayersByDefault: true,
    },
    purge: {},
    theme: {
      extend: {
        colors: {
          primary:"var(--primaryBrandingColor)",
          accent:"var(--accentBrandingColor)",
          dark:"var(--darkBrandingColor)",
          light:"var(--lightBrandingColor)",
          text:"var(--defaultTextColor)",
          offwhite:"var(--lightTextColor)"
        }
      },  
    },
    variants: {},
    plugins: [],
  }
  